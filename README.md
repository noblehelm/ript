# ript

*ript* (**r**ust **i**mage **p**rocessing **t**oolbox) is (supposed to be) a library for image processing functions, which includes functions for transformation, interpolation and etc..

The library is currently in **ALPHA** state, that is, it lacks many, many, *many* features, documentation and test cases, so please DO **NOT** USE it production, for now..
# USAGE

Include both the `ript` crate and the `image` crate on your Cargo.toml file

```
ript = "*"
image = "0.19"
```

## File opening

To open an image to work with, you must use `image`'s' `open` function:

```
use image;

fn main(){
	let img = image::open("test.jpg").unwrap();
}
```

There is also a `save` function to save to a file when your work is done:

```
img.save("test_after.jpg").unwrap();
```

## Trait usage

To make use of traits, you must first put it into scope.

```
use ript::Transformation;
```

After that, you'll be able to use the trait-related functions available in the specific trait that was pushed onto scope.

```
use ript::Transformation;

fn main() {
	let img = image::open("test.jpg").unwrap();
	let negative_img = img.negative(); // inverts every pixel in the image, to give a 'negative' of the image
}
```

# Additional note

`image` crate opening results in a `DynamicImage` type, which implements its own `GenericImage` trait. But most of the traits implemented in this library are implemented upon the types like `RgbImage` (from `image` crate). Therefore, to properly use the traits, one must cast them onto the specific types using `to` functions.

```
use image;

fn main() {
	let img = image::open("test.jpg").unwrap();
	let rgb_img = img.to_rbg(); // will return the image::RgbImage type
	let luma_img = img.to_luma(); // will return the image::GrayImage type, that is, a grayscale image
}
```

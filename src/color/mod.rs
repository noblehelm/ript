pub mod pixel;
use crate::color::pixel::cmy::Cmy;
use crate::color::pixel::cmyk::Cmyk;
use crate::color::pixel::hsi::Hsi;

use image::{Primitive, RgbImage};
use num::{zero, Float};

pub struct PixelIterator<T: Iterator> {
    iter: T
}

impl<T: Iterator> Iterator for PixelIterator<T> {
    type Item = T;

    fn next(&mut self) -> Option<Self::Item> {
        self.next()
    }
}

macro_rules! container {
    {$(
            $color_space:ident,
            $color_container:ident,
            $trait_bound:ident
            $(, $color_component:ident)+;

    )*} => {
        $(
            #[derive(Clone)]
            pub struct $color_container<T: $trait_bound> {
                width: u32,
                height: u32,
                iter_index: usize,
                data: Vec<$color_space<T>>
            }

            impl<T: $trait_bound + 'static> $color_container<T> {
                fn new(w: u32, h: u32) -> Self {
                    $color_container {
                        width: w,
                        height: h,
                        iter_index: 0usize,
                        data: vec![$color_space {
                            $(
                                $color_component: zero(),
                                )+
                        }; (w * h) as usize],
                    }
                }

                /// Returns the width of the image
                fn width(&self) -> u32 {
                    self.width
                }

                /// Returns the height of the image
                fn height(&self) -> u32 {
                    self.height
                }

                /// Returns the pixel contained in the given position
                fn pixel(&self, x: u32, y: u32) -> $color_space<T> {
                    self.data[(x as usize * self.width as usize) + y as usize]
                }

                /// Puts a given pixel into the specificed coordinates in the image
                fn set_pixel(&mut self, x: u32, y: u32, pix: $color_space<T>) {
                    self.data[(x as usize * self.width as usize) + y as usize] = pix;
                }

                /// Returns the vector containing all pixels directly
                fn into_raw(self) -> Vec<$color_space<T>> {
                    self.data
                }

                /// Creates a new image with a given vector containing the pixels
                ///
                /// Pixels must correspond to the image container, otherwise the rust compiler
                /// will complain of type error.
                fn from_raw(w: u32, h: u32, raw: &[$color_space<T>]) -> Self {
                    $color_container {
                        width: w,
                        height: h,
                        iter_index: 0usize,
                        data: raw.to_vec()
                    }
                }

                // fn iter(&self) -> PixelIterator<$color_space<T>> {
                //     PixelIterator { iter: self.data.iter() }
                // }
            }

            )*
    }
}

            //impl<T> Iterator for $color_container<T> where T: $trait_bound {
            //    type Item = $color_space<T>;

            //    fn next(&mut self) -> Option<Self::Item> {
            //        if self.iter_index < self.data.len() {
            //            self.iter_index += 1;
            //            Some(self.data[self.iter_index - 1])
            //        } else {
            //            None
            //        }
            //    }
            //}
container!{
    Cmy, CmyImage, Primitive, c, m, y;
    Hsi, HsiImage, Float, h, s, i;
    Cmyk, CmykImage, Float, c, m, y, k;
}

macro_rules! impl_from {
    ($(
            $original:ident,
            $result:ident,
            $result_pixel:ident,
            $generic_type:ty;
    )*) => {$(
            impl From<$original> for $result<$generic_type> {
                fn from(image: $original) -> Self {
                    let mut result = $result::new(image.width(), image.height());
                    for (x, y, pix) in image.enumerate_pixels() {
                        result.set_pixel(x, y, $result_pixel::from(*pix));
                    }
                    result
                }
            }
    )*}
}

impl_from! {
    RgbImage, CmyImage, Cmy, u8;
    RgbImage, HsiImage, Hsi, f32;
    RgbImage, CmykImage, Cmyk, f32;
}

macro_rules! impl_into {
    ($(
            $original:ident,
            $result:ident,
            $result_pixel:ident,
            $original_generic_type:ty,
            $result_generic_type:ty;
    )*) => {$(
            impl Into<$result> for $original<$original_generic_type> {
                fn into(self) -> $result {
                    let mut result = $result::new(self.width(), self.height());
                    for (x, y, pix) in self.enumerate_pixels() {
                        let temp: $result_pixel<$result_generic_type> = pix.into();
                        result.put_pixel(x, y, temp);
                    }
                    result
                }
            }
    )*}
}

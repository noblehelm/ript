use image::{Primitive, Rgb};
use num::zero;

#[derive(Debug, Clone, Copy)]
pub struct Cmy<T: Primitive> {
    pub c: T,
    pub m: T,
    pub y: T,
}

impl<T: Primitive> Cmy<T> {
    fn new() -> Self {
        Cmy {
            c: zero(),
            m: zero(),
            y: zero(),
        }
    }
}

impl<T: Primitive> From<Rgb<T>> for Cmy<T> {
    fn from(pixel: Rgb<T>) -> Self {
        Cmy {
            c: T::max_value() - pixel[0],
            m: T::max_value() - pixel[1],
            y: T::max_value() - pixel[2],
        }
    }
}

impl<T: Primitive> Into<Rgb<T>> for Cmy<T> {
    fn into(self) -> Rgb<T> {
        Rgb([
            T::max_value() - self.c,
            T::max_value() - self.m,
            T::max_value() - self.y,
        ])
    }
}

#[cfg(test)]
mod tests {
    use crate::color::pixel::cmy::Cmy;
    use image::Rgb;

    #[test]
    fn test_cmy_from_rgb() {
        let test: Rgb<u8> = Rgb([0, 0, 0]);
        let cmy_test: Cmy<u8> = Cmy::from(test);
        assert_eq!(cmy_test.c, 255);
        assert_eq!(cmy_test.m, 255);
        assert_eq!(cmy_test.y, 255);
    }

    #[test]
    fn test_rgb_into_cmy() {
        let test: Rgb<u8> = Rgb([255, 255, 255]);
        let cmy_test: Cmy<u8> = test.into();
        assert_eq!(cmy_test.c, 0);
        assert_eq!(cmy_test.m, 0);
        assert_eq!(cmy_test.y, 0);
    }

    #[test]
    fn test_cmy_into_rgb() {
        let test: Cmy<u8> = Cmy {
            c: 255,
            m: 255,
            y: 255,
        };
        let rgb_test: Rgb<u8> = test.into();
        assert_eq!(rgb_test[0], 0);
        assert_eq!(rgb_test[1], 0);
        assert_eq!(rgb_test[2], 0);
    }
}

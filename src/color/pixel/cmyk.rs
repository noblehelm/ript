use image::{Primitive, Rgb};
use num::{zero, Float};

#[derive(Debug, Clone, Copy)]
pub struct Cmyk<T: Float> {
    pub c: T,
    pub m: T,
    pub y: T,
    pub k: T,
}

impl<T: Float> Cmyk<T> {
    fn new() -> Self {
        Cmyk {
            c: zero(),
            m: zero(),
            y: zero(),
            k: zero(),
        }
    }
}

impl<T, U> From<Rgb<U>> for Cmyk<T>
where
    T: Float,
    U: Primitive,
{
    fn from(pixel: Rgb<U>) -> Self {
        let r_quote = T::from(pixel[0]).unwrap() / T::from(U::max_value()).unwrap();
        let g_quote = T::from(pixel[1]).unwrap() / T::from(U::max_value()).unwrap();
        let b_quote = T::from(pixel[2]).unwrap() / T::from(U::max_value()).unwrap();
        let k = T::from(1).unwrap() - T::max(r_quote, T::max(g_quote, b_quote));
        if r_quote == g_quote && r_quote == b_quote {
            Cmyk {
                c: T::from(0).unwrap(),
                m: T::from(0).unwrap(),
                y: T::from(0).unwrap(),
                k: k,
            }
        } else {
            Cmyk {
                c: (T::from(1).unwrap() - r_quote - k) / (T::from(1).unwrap() - k),
                m: (T::from(1).unwrap() - g_quote - k) / (T::from(1).unwrap() - k),
                y: (T::from(1).unwrap() - b_quote - k) / (T::from(1).unwrap() - k),
                k: k,
            }
        }
    }
}

impl<T, U> Into<Rgb<U>> for Cmyk<T>
where
    T: Float,
    U: Primitive,
{
    fn into(self) -> Rgb<U> {
        Rgb([
            U::max_value()
                * (U::from(1).unwrap() - U::from(self.c).unwrap())
                * (U::from(1).unwrap() - U::from(self.k).unwrap()),
            U::max_value()
                * (U::from(1).unwrap() - U::from(self.m).unwrap())
                * (U::from(1).unwrap() - U::from(self.k).unwrap()),
            U::max_value()
                * (U::from(1).unwrap() - U::from(self.y).unwrap())
                * (U::from(1).unwrap() - U::from(self.k).unwrap()),
        ])
    }
}

use image::{Primitive, Rgb};
use num::{zero, Float};
use std::u8;

/// HSI color space, which stands for Hue, Saturation and Intensity
#[derive(Debug, Clone, Copy)]
pub struct Hsi<T: Float> {
    pub h: T,
    pub s: T,
    pub i: T,
}

impl<T: Float> Hsi<T> {
    /// Create and returns a new, zero-ed Hsi pixel
    fn new() -> Self {
        Hsi {
            h: zero(),
            s: zero(),
            i: zero(),
        }
    }

    /// Converts the given pixel to RGB color space
    pub fn to_rgb(&self) -> Rgb<u8> {
        match (self.h * T::from(360u32).unwrap()).to_u32() {
            Some(0...119) => {
                let b = self.i * (T::from(1).unwrap() - self.s);
                let r = self.i
                    * (T::from(1).unwrap()
                        + (self.s * self.h.cos().to_degrees())
                            / (T::from(60).unwrap() - self.h).cos().to_degrees());
                let g = T::from(3).unwrap() * self.i - (r + b);
                Rgb([
                    r.to_u8().unwrap_or(0u8),
                    g.to_u8().unwrap_or(0u8),
                    b.to_u8().unwrap_or(0u8),
                ])
            }
            Some(120...239) => {
                let h = self.h - T::from(120).unwrap();
                let r = self.i * (T::from(1).unwrap() - self.s);
                let g = self.i
                    * (T::from(1).unwrap()
                        + (self.s * h.cos().to_degrees()
                            / (T::from(60).unwrap() - h).cos().to_degrees()));
                let b = T::from(3).unwrap() * self.i - (r + g);
                Rgb([
                    r.to_u8().unwrap_or(0u8),
                    g.to_u8().unwrap_or(0u8),
                    b.to_u8().unwrap_or(0u8),
                ])
            }
            Some(240...360) => {
                let h = self.h - T::from(240).unwrap();
                let g = self.i * (T::from(1).unwrap() - self.s);
                let b = self.i
                    * (T::from(1).unwrap()
                        + (self.s * h.cos().to_degrees())
                            / (T::from(60).unwrap() - h).cos().to_degrees());
                let r = T::from(3).unwrap() * self.i - (g + b);
                Rgb([
                    r.to_u8().unwrap_or(0u8),
                    g.to_u8().unwrap_or(0u8),
                    b.to_u8().unwrap_or(0u8),
                ])
            }
            _ => Rgb([0u8, 0u8, 0u8]),
        }
    }
}

impl<T, U> From<Rgb<U>> for Hsi<T>
where
    T: Float,
    U: Primitive,
{
    fn from(pixel: Rgb<U>) -> Self {
        let r = T::from(pixel[0]).unwrap() / T::from(U::max_value()).unwrap();
        let g = T::from(pixel[1]).unwrap() / T::from(U::max_value()).unwrap();
        let b = T::from(pixel[2]).unwrap() / T::from(U::max_value()).unwrap();
        let i = (r + g + b) / T::from(3).unwrap();
        let s =
            T::from(1).unwrap() - (T::from(3).unwrap() / (r + g + b)) * (T::min(r, T::min(g, b)));
        let mut h = zero();
        if g <= b {
            h = theta(r, g, b) / T::from(360).unwrap();
        } else {
            h = (T::from(360).unwrap() - theta(r, g, b)) / T::from(360).unwrap();
        }
        Hsi { h: h, s: s, i: i }
    }
}

fn theta<T: Float>(r: T, g: T, b: T) -> T {
    if r == b && r == g {
        T::from(0).unwrap()
    } else {
        (((r - g) + (r - b)) / T::from(2).unwrap() / T::sqrt((r - g).powi(2) + (r - b) * (g - b))
            + T::from(0.000001).unwrap()).acos()
        .to_degrees()
    }
}

#[cfg(test)]
mod tests {}

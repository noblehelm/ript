use crate::color::pixel::hue::hue;
use image::{Primitive, Rgb};
use num::{zero, Float};

#[derive(Debug, Clone, Copy)]
pub struct Hsv<T: Float> {
    pub h: T,
    pub s: T,
    pub v: T,
}

impl<T: Float> Hsv<T> {
    fn new() -> Self {
        Hsv {
            h: zero(),
            s: zero(),
            v: zero(),
        }
    }
}

// A "type-agnostic" From conversion from Rgb color-space to Hsv color-space. The Rgb values are
// normalized, so the type U can be assumed (to be) from u8 to u64, or f32 to f64, while the type T
// must be a floating point type, either f32 or f64 if Rust primitives.
impl<T, U> From<Rgb<U>> for Hsv<T>
where
    T: Float,
    U: Primitive,
{
    fn from(pixel: Rgb<U>) -> Self {
        let r = T::from(pixel[0]).unwrap() / T::from(U::max_value()).unwrap();
        let g = T::from(pixel[1]).unwrap() / T::from(U::max_value()).unwrap();
        let b = T::from(pixel[2]).unwrap() / T::from(U::max_value()).unwrap();
        let c_max = T::max(r, T::max(g, b));
        let c_min = T::min(r, T::min(g, b));
        let delta = c_max - c_min;
        let _zero: T = zero();
        Hsv {
            h: hue(r, g, b, delta),
            s: match c_max {
                zero => zero,
                _ => delta / c_max,
            },
            v: c_max,
        }
    }
}

//impl<T, U> Into<Rgb<U>> for Hsv<T>
//where T: Float, U: Primitive {
//    fn into(self) -> Rgb<U> {
//        let h: U;
//        let s: U;
//        let v: U;
//        if let Some(value) = U::from(self.h) { h = value; }
//        if let Some(value) = U::from(self.s) { s = value; }
//        if let Some(value) = U::from(self.v) { v = value; }
//        let c = v * s;
//        let x = c * (U::from(1).unwrap() - U::abs((h / U::from(60).unwrap()) % U::from(2).unwrap() - U::from(1).unwrap()));
//        let m = v - c;
//        let quote = match h {
//
//        };
//        Rgb([
//            (quote[0] + m) * U::from(255).unwrap(),
//            (quote[1] + m) * U::from(255).unwrap(),
//            (quote[1] + m) * U::from(255).unwrap(),
//        ])
//    }
//}

use num::Float;

/// Generate hue based on RGB values provided.
///
/// It is based on the circular Hue model, that ranges from 0º to 360º degrees (not radians). The
/// output is the resulting degree (e.g.: 300º) divided by 360, to normalize the output into the
/// [0:1] range. It is used by two Hue-based models: HSV and HSL.
pub fn hue<T: Float>(r: T, g: T, b: T, delta: T) -> T {
    if delta == T::from(0).unwrap() {
        T::from(0).unwrap()
    } else {
        let cmax = T::max(r, T::max(g, b));
        match cmax {
            _r => T::from(60).unwrap() * ((g - b) / delta % T::from(6).unwrap()),
            _g => T::from(60).unwrap() * ((b - r) / delta + T::from(2).unwrap()),
            g => T::from(60).unwrap() * ((r - g) / delta + T::from(4).unwrap()),
            _ => T::from(0).unwrap(),
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::color::pixel::hue;

    #[test]
    fn test_hue() {
        assert_eq!(hue::hue(1f32, 0f32, 1f32, 1f32), 300f32);
    }
}

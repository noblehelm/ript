use num::Float;

#[derive(Debug, Clone, Copy)]
pub struct Lab<T: Float> {
    pub l: T,
    pub a: T,
    pub b: T,
}

impl<T: Float> Lab<T> {
    fn new() -> Self {
        Lab {
            l: zero(),
            a: zero(),
            b: zero(),
        }
    }
}

impl<T, U> From<Xyz<U>> for Lab<T> {
    fn from(pixel: Xyz<U>) -> Self {

    }
}

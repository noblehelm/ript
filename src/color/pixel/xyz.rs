use image::Rgb;
use num::{zero, Float};

#[derive(Debug, Clone, Copy)]
pub struct Xyz<T: Float> {
    pub x: T,
    pub y: T,
    pub z: T,
}

impl<T: Float> Xyz<T> {
    fn new() -> Self {
        Xyz {
            x: zero(),
            y: zero(),
            z: zero(),
        }
    }
}

impl From<Rgb<u8>> for Xyz<f32> {
    fn from(pixel: Rgb<u8>) -> Self {
        let (mut r, mut g, mut b) = (
            pixel[0] as f32 / u8::max_value() as f32,
            pixel[1] as f32 / u8::max_value() as f32,
            pixel[2] as f32 / u8::max_value() as f32,
        );
        r = if r > 0.04045_f32 {
            ((r + 0.055_f32) / 1.055_f32).powf(2.4_f32)
        } else {
            r / 12.92_f32
        };
        g = if g > 0.04045_f32 {
            ((g + 0.055_f32) / 1.055_f32).powf(2.4_f32)
        } else {
            g / 12.92_f32
        };
        b = if b > 0.04045_f32 {
            ((b + 0.055_f32) / 1.055_f32).powf(2.4_f32)
        } else {
            b / 12.92_f32
        };
        r *= 100_f32;
        g *= 100_f32;
        b *= 100_f32;
        Xyz {
            x: r * 0.4124_f32 + g * 0.3576_f32 + b * 0.1805_f32,
            y: r * 0.2126_f32 + g * 0.7152_f32 + b * 0.0722_f32,
            z: r * 0.0193_f32 + g * 0.1192_f32 + b * 0.9505_f32,
        }
    }
}

impl Into<Rgb<u8>> for Xyz<f32> {
    fn into(self) -> Rgb<u8> {
        let (x, y, z) = (self.x / 100f32, self.y / 100f32, self.z / 100f32);
        let mut r = (3.2404542_f32) * x + (-1.5371385_f32) * y + (-0.4985314_f32) * z;
        let mut g = (-0.9692660_f32) * x + (1.8760108_f32) * y + (0.0415560_f32) * z;
        let mut b = (0.0556434_f32) * x + (-0.2040259_f32) * y + (1.0572252_f32) * z;
        r = if r > 0.0031308_f32 {
            1.055_f32 * (r.powf(1_f32 / 2.4_f32)) - 0.055_f32
        } else {
            12.92_f32 * r
        };
        g = if g > 0.0031308_f32 {
            1.055_f32 * (g.powf(1_f32 / 2.4_f32)) - 0.055_f32
        } else {
            12.92_f32 * g
        };
        b = if b > 0.0031308_f32 {
            1.055_f32 * (b.powf(1_f32 / 2.4_f32)) - 0.055_f32
        } else {
            12.92_f32 * b
        };
        Rgb([
            (r * u8::max_value() as f32) as u8,
            (g * u8::max_value() as f32) as u8,
            (b * u8::max_value() as f32) as u8,
        ])
    }
}

#[cfg(test)]
mod tests {
    use crate::color::pixel::xyz;
    use image::Rgb;

    #[test]
    fn test_xyz_from_rgb() {
        let pixel1: Rgb<u8> = Rgb([255, 255, 255]);
        let pixel2: xyz::Xyz<f32> = xyz::Xyz::from(pixel1);
        assert_eq!(pixel2.x, 95.05f32);
        assert_eq!(pixel2.y, 100.0f32);
        assert_eq!(pixel2.z, 108.9f32);
    }

    #[test]
    fn test_xyz_into_rgb() {
        let pixel1 = xyz::Xyz {
            x: 90.0471f32,
            y: 100f32,
            z: 108.883f32,
        };
        let pixel2: Rgb<u8> = pixel1.into();
        assert_eq!(pixel2[0], 235);
        assert_eq!(pixel2[1], 4);
        assert_eq!(pixel2[2], 254);
    }
}

use image::{Rgb, RgbImage};
use crate::traits::neighborhood::Neighborhood;

pub trait Smoothing {
    fn lowpass(&self, n: NeighborhoodRange) -> Self;
    fn weighted_lowpass(&self, mask: Vec<u8>) -> Self;
}

enum NeighborhoodRange {
    Three,
    Five,
    Seven,
}

impl Smoothing for RgbImage {
    fn lowpass(&self, n: NeighborhoodRange) -> Self {
        let mut result = self.clone();
        for (x, y, _pix) in self.enumerate_pixels() {
            let r = match n {
                NeighborhoodRange::Three => {
                    self.three_by_three(x, y)
                        .into_iter()
                        .map(|x| x[0])
                        .sum::<u8>()
                        / 9
                }
                NeighborhoodRange::Five => {
                    self.five_by_five(x, y)
                        .into_iter()
                        .map(|x| x[0])
                        .sum::<u8>()
                        / 25
                }
                NeighborhoodRange::Seven => {
                    self.seven_by_seven(x, y)
                        .into_iter()
                        .map(|x| x[0])
                        .sum::<u8>()
                        / 49
                }
            };
            let g = match n {
                NeighborhoodRange::Three => {
                    self.three_by_three(x, y)
                        .into_iter()
                        .map(|x| x[1])
                        .sum::<u8>()
                        / 9
                }
                NeighborhoodRange::Five => {
                    self.five_by_five(x, y)
                        .into_iter()
                        .map(|x| x[1])
                        .sum::<u8>()
                        / 25
                }
                NeighborhoodRange::Seven => {
                    self.seven_by_seven(x, y)
                        .into_iter()
                        .map(|x| x[1])
                        .sum::<u8>()
                        / 49
                }
            };
            let b = match n {
                NeighborhoodRange::Three => {
                    self.three_by_three(x, y)
                        .into_iter()
                        .map(|x| x[2])
                        .sum::<u8>()
                        / 9
                }
                NeighborhoodRange::Five => {
                    self.five_by_five(x, y)
                        .into_iter()
                        .map(|x| x[2])
                        .sum::<u8>()
                        / 25
                }
                NeighborhoodRange::Seven => {
                    self.seven_by_seven(x, y)
                        .into_iter()
                        .map(|x| x[2])
                        .sum::<u8>()
                        / 49
                }
            };
            result.put_pixel(x, y, Rgb([r, g, b]));
        }
        result
    }

    fn weighted_lowpass(&self, _mask: Vec<u8>) -> Self {
        panic!("Not implemented yet!");
    }
}

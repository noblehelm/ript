/// Histogram data for grayscale images
pub struct LumaDistribution {
    pub l: Vec<u32>,
    pub pixel_count: u32,
}

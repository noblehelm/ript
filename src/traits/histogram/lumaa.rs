/// Histogram data for Grayscale images with an alpha channel
pub struct LumaAlphaDistribution {
    pub l: Vec<u32>,
    pub a: Vec<u32>,
    pub pixel_count: u32,
}

pub mod luma;
pub mod lumaa;
pub mod rgb;
pub mod rgba;

use image::{GrayAlphaImage, GrayImage, RgbImage, RgbaImage};

pub trait Histogram {
    type Dist;

    fn generate(&self) -> Self::Dist;
}

impl Histogram for GrayImage {
    type Dist = luma::LumaDistribution;

    fn generate(&self) -> Self::Dist {
        let mut hist = luma::LumaDistribution {
            l: vec![0; <u8>::max_value() as usize],
            pixel_count: self.pixels().count() as u32,
        };
        for pix in self.pixels() {
            hist.l[pix[0] as usize] += 1;
        }
        hist
    }
}

impl Histogram for GrayAlphaImage {
    type Dist = lumaa::LumaAlphaDistribution;

    fn generate(&self) -> Self::Dist {
        let mut hist = lumaa::LumaAlphaDistribution {
            l: vec![0; <u8>::max_value() as usize],
            a: vec![0; <u8>::max_value() as usize],
            pixel_count: self.pixels().count() as u32,
        };
        for pix in self.pixels() {
            hist.l[pix[0] as usize] += 1;
            hist.a[pix[1] as usize] += 1;
        }
        hist
    }
}

impl Histogram for RgbImage {
    type Dist = rgb::RgbDistribution;

    fn generate(&self) -> Self::Dist {
        let mut hist = rgb::RgbDistribution {
            r: vec![0; <u8>::max_value() as usize],
            g: vec![0; <u8>::max_value() as usize],
            b: vec![0; <u8>::max_value() as usize],
            pixel_count: self.pixels().count() as u32,
        };
        for pix in self.pixels() {
            hist.r[pix[0] as usize] += 1;
            hist.g[pix[1] as usize] += 1;
            hist.b[pix[2] as usize] += 1;
        }
        hist
    }
}

impl Histogram for RgbaImage {
    type Dist = rgba::RgbaDistribution;

    fn generate(&self) -> Self::Dist {
        let mut hist = rgba::RgbaDistribution {
            r: vec![0; <u8>::max_value() as usize],
            g: vec![0; <u8>::max_value() as usize],
            b: vec![0; <u8>::max_value() as usize],
            a: vec![0; <u8>::max_value() as usize],
            pixel_count: self.pixels().count() as u32,
        };
        for pix in self.pixels() {
            hist.r[pix[0] as usize] += 1;
            hist.g[pix[1] as usize] += 1;
            hist.b[pix[2] as usize] += 1;
            hist.a[pix[3] as usize] += 1;
        }
        hist
    }
}

//untested, legacy, will use for later
/*pub fn histogram_equalization(hist: &Vec<Frequency>) -> Vec<Frequency> {
	/// Equalizes histogram using the cumulative distribution function (CDF)
	/// For the time being, assumes a histogram of a GRAYSCALE image, since it
	/// only works with a single channel.
	let mut hist_equalized = Vec::with_capacity(hist.len());
	// hardcoded 8-bit rgb, will need to change later
	let pixel = 256;
	for ( i, h ) in (0..pixel).enumerate(){
		if h == 0 {
			hist_equalized.push(Frequency(hist[i].0, hist[i].1 * pixel))
		}
		else {
			hist_equalized.push(Frequency(hist[i].0, hist[i].1 * pixel + hist_equalized[i - 1]))
		}
	}
	hist_equalized
}*/

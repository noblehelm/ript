/// Histogram data for Rgb images
pub struct RgbDistribution {
    pub r: Vec<u32>,
    pub g: Vec<u32>,
    pub b: Vec<u32>,
    pub pixel_count: u32,
}

/// Histogram data for Rgb images with an alpha channel
pub struct RgbaDistribution {
    pub r: Vec<u32>,
    pub g: Vec<u32>,
    pub b: Vec<u32>,
    pub a: Vec<u32>,
    pub pixel_count: u32,
}

use image;

pub trait LumaInterpolation {
	fn bilinear(image: &image::GrayImage, x: u32, y: u32) -> image::Luma<u8>;
}

impl LumaInterpolation for image::Luma<u8> {
	fn bilinear(image: &image::GrayImage, x: u32, y: u32) -> image::Luma<u8> {
		let mut l: u8 = 0;
		let mut count: u8 = 0;
		if x >= 1 {
			l += image.get_pixel(x - 1, y)[0];
			count += 1;
		}
		if y >= 1 {
			l += image.get_pixel(x, y - 1)[0];
			count += 1;
		}
		if x <= image.width() {
			l += image.get_pixel(x + 1, y)[0];
			count += 1;
		}
		if y <= image.height() {
			l += image.get_pixel(x, y + 1)[0];
			count += 1;
		}
		let new_pixel: image::Luma<u8> = image::Luma([l / count]);
		new_pixel
	}
}

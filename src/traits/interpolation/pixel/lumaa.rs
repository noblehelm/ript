use image;

pub trait LumaAInterpolation {
    fn bilinear(image: &image::GrayAlphaImage, x: u32, y: u32) -> image::LumaA<T>;
}

impl LumaAInterpolation for image::LumaA<T> {
    fn bilinear(image: &image::GrayAlphaImage, x: u32, y: u32) -> image::LumaA<T> {
        let mut l: u8 = 0;
        let mut a: u8 = 0;
        let mut count: u8 = 0;
        if x >= 1 {
            l += image.get_pixel(x - 1, y)[0];
            a += image.get_pixel(x - 1, y)[1];
            count += 1;
        }
        if y >= 1 {
            l += image.get_pixel(x, y - 1)[0];
            a += image.get_pixel(x, y - 1)[1];
            count += 1;
        }
        if x <= image.width() {
            l += image.get_pixel(x + 1, y)[0];
            a += image.get_pixel(x + 1, y)[1];
            count += 1;
        }
        if y <= image.height() {
            l += image.get_pixel(x, y + 1)[0];
            a += image.get_pixel(x, y + 1)[1];
            count += 1;
        }
        let new_pixel: image::LumaA<u8> = image::LumaA([l / count, a / count]);
        new_pixel
    }
}

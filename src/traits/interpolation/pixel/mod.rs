//pub mod luma;
//pub mod lumaa;
//pub mod rgb;
//pub mod rgba;

use image;

pub trait PixelInterpolation {
    type Pixel;

    fn bilinear_pixel_interpol(&self, x: u32, y: u32) -> Self::Pixel;
}

macro_rules! interpol {
    {
        $image_container:ty,
        $return_type:ty,
        $pixel_container:expr,
        $primitive:ty $(, ($colour:ident, $number:expr))+
    } => {
        impl PixelInterpolation for $image_container {
            type Pixel = $return_type;
            fn bilinear_pixel_interpol(&self, x: u32, y: u32) -> $return_type {
                $(
                    let mut $colour: $primitive = 0;
                )+
                    let mut count: $primitive = 0;
                if x >= 1 {
                    $(
                        $colour += self.get_pixel(x - 1, y)[$number];
                        count += 1;
                    )+
                }
                if y >= 1 {
                    $(
                        $colour += self.get_pixel(x, y - 1)[$number];
                        count += 1;
                    )+
                }
                if x <= self.width() {
                    $(
                        $colour += self.get_pixel(x + 1, y)[$number];
                        count += 1;
                    )+
                }
                if y <= self.height() {
                    $(
                        $colour += self.get_pixel(x, y + 1)[$number];
                        count += 1;
                    )+
                }
                $pixel_container([$($colour/count),+])
            }
        }
    }

}

interpol!{
    image::RgbImage, image::Rgb<u8>, image::Rgb, u8, (r, 0), (g, 1), (b, 2)
}
interpol!{
    image::RgbaImage, image::Rgba<u8>, image::Rgba, u8, (r, 0), (g, 1), (b, 2), (a, 3)
}
interpol!{
    image::GrayImage, image::Luma<u8>, image::Luma, u8, (l, 0)
}
interpol!{
    image::GrayAlphaImage, image::LumaA<u8>, image::LumaA, u8, (l, 0), (a, 1)
}

use image;

pub trait RgbInterpolation {
    fn bilinear(image: &image::RgbImage, x: u32, y: u32) -> image::Rgb<u8>;
}

impl RgbInterpolation for image::Rgb<u8> {
    fn bilinear(image: &image::RgbImage, x: u32, y: u32) -> image::Rgb<u8> {
        let mut r: u8 = 0;
        let mut g: u8 = 0;
        let mut b: u8 = 0;
        let mut count: u8 = 0;
        if x >= 1 {
            r += image.get_pixel(x - 1, y)[0];
            g += image.get_pixel(x - 1, y)[1];
            b += image.get_pixel(x - 1, y)[2];
            count += 1;
        }
        if x >= 1 {
            r += image.get_pixel(x, y - 1)[0];
            g += image.get_pixel(x, y - 1)[1];
            b += image.get_pixel(x, y - 1)[2];
            count += 1;
        }
        if x <= image.width() {
            r += image.get_pixel(x + 1, y)[0];
            g += image.get_pixel(x + 1, y)[1];
            b += image.get_pixel(x + 1, y)[2];
            count += 1;
        }
        if y <= image.height() {
            r += image.get_pixel(x, y + 1)[0];
            g += image.get_pixel(x, y + 1)[1];
            b += image.get_pixel(x, y + 1)[2];
            count += 1;
        }
        let new_pixel = image::Rgb([r / count, g / count, b / count]);
        new_pixel
    }
}

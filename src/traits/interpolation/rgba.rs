use image;

pub trait RgbaInterpolation {
	fn bilinear(image: &image::RgbaImage, x: u32, y: u32) -> image::Rgba<u8>;
}

impl RgbaInterpolation for image::Rgba<u8> {
	fn bilinear(image: &image::RgbaImage, x: u32, y: u32) -> image::Rgba<u8>{
		let mut r: u8 = 0;
		let mut g: u8 = 0;
		let mut b: u8 = 0;
		let mut a: u8 = 0;
		let mut count: u8 = 0;
		if x >= 1 {
			r += image.get_pixel(x - 1, y)[0];
			g += image.get_pixel(x - 1, y)[1];
			b += image.get_pixel(x - 1, y)[2];
			a += image.get_pixel(x - 1, y)[3];
			count += 1;
		}
		if y >= 1 {
			r += image.get_pixel(x, y - 1)[0];
			g += image.get_pixel(x, y - 1)[1];
			b += image.get_pixel(x, y - 1)[2];
			a += image.get_pixel(x, y - 1)[3];
			count += 1;
		}
		if x >= 1 {
			r += image.get_pixel(x + 1, y)[0];
			g += image.get_pixel(x + 1, y)[1];
			b += image.get_pixel(x + 1, y)[2];
			a += image.get_pixel(x + 1, y)[3];
			count += 1;
		}
		if x >= 1 {
			r += image.get_pixel(x, y + 1)[0];
			g += image.get_pixel(x, y + 1)[1];
			b += image.get_pixel(x, y + 1)[2];
			a += image.get_pixel(x, y + 1)[3];
			count += 1;
		}
		let new_pixel = image::Rgba([r / count, g / count, b / count, a / count]);
		new_pixel
	}
}

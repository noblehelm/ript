use image::{GrayAlphaImage, GrayImage, Luma, LumaA, Rgb, RgbImage, Rgba, RgbaImage};

pub trait Misc {
    fn to_black(self) -> Self;
    fn to_white(self) -> Self;
}

pub trait MiscAlpha {
    fn to_transparent(self) -> Self;
}

impl Misc for RgbImage {
    fn to_black(self) -> Self {
        let mut result = self.clone();
        for (x, y, _pix) in self.enumerate_pixels() {
            result.put_pixel(x, y, Rgb([0, 0, 0]));
        }
        result
    }

    fn to_white(self) -> Self {
        let mut result = self.clone();
        for (x, y, _pix) in self.enumerate_pixels() {
            result.put_pixel(x, y, Rgb([255, 255, 255]));
        }
        result
    }
}

impl Misc for RgbaImage {
    fn to_black(self) -> Self {
        let mut result = self.clone();
        for (x, y, _pix) in self.enumerate_pixels() {
            result.put_pixel(x, y, Rgba([0, 0, 0, 255]));
        }
        result
    }

    fn to_white(self) -> Self {
        let mut result = self.clone();
        for (x, y, _pix) in self.enumerate_pixels() {
            result.put_pixel(x, y, Rgba([255, 255, 255, 255]));
        }
        result
    }
}

impl Misc for GrayImage {
    fn to_black(self) -> Self {
        let mut result = self.clone();
        for (x, y, _pix) in self.enumerate_pixels() {
            result.put_pixel(x, y, Luma([0]));
        }
        result
    }

    fn to_white(self) -> Self {
        let mut result = self.clone();
        for (x, y, _pix) in self.enumerate_pixels() {
            result.put_pixel(x, y, Luma([255]));
        }
        result
    }
}

impl Misc for GrayAlphaImage {
    fn to_black(self) -> Self {
        let mut result = self.clone();
        for (x, y, _pix) in self.enumerate_pixels() {
            result.put_pixel(x, y, LumaA([0, 255]));
        }
        result
    }

    fn to_white(self) -> Self {
        let mut result = self.clone();
        for (x, y, _pix) in self.enumerate_pixels() {
            result.put_pixel(x, y, LumaA([255, 255]));
        }
        result
    }
}

impl MiscAlpha for RgbaImage {
    fn to_transparent(self) -> Self {
        let mut transparent_image = self.clone();
        for (x, y, _pix) in self.enumerate_pixels() {
            transparent_image.put_pixel(x, y, Rgba([0, 0, 0, 0]));
        }
        transparent_image
    }
}

impl MiscAlpha for GrayAlphaImage {
    fn to_transparent(self) -> Self {
        let mut transparent_image = self.clone();
        for (x, y, _pix) in self.enumerate_pixels() {
            transparent_image.put_pixel(x, y, LumaA([0, 0]));
        }
        transparent_image
    }
}

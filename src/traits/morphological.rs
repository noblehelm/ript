use image;

use crate::traits::misc::{Misc, MiscAlpha};

pub trait Morphological {
    fn reflection(self) -> Self;
    fn translation(&self, move_x: u32, move_y: u32, background: &str) -> Self;
}

impl Morphological for image::RgbImage {
    fn reflection(self) -> Self {
        let mut reflected_image = self.clone();
        for (x, y, pix) in self.enumerate_pixels() {
            reflected_image.put_pixel((self.width() - 1) - x, (self.height() - 1) - y, *pix);
        }
        reflected_image
    }

    fn translation(&self, move_x: u32, move_y: u32, background: &str) -> Self {
        let mut translated_image = self.clone();
        translated_image = match background {
            "black" => translated_image.to_black(),
            "white" => translated_image.to_white(),
            _ => translated_image.to_black(),
        };
        for (x, y, pix) in self.enumerate_pixels() {
            if x + move_x < self.width() && y + move_y < self.height() {
                translated_image.put_pixel(x + move_x, y + move_y, *pix);
            }
        }
        translated_image
    }
}

impl Morphological for image::RgbaImage {
    fn reflection(self) -> Self {
        let mut reflected_image = self.clone();
        for (x, y, pix) in self.enumerate_pixels() {
            reflected_image.put_pixel((self.width() - 1) - x, (self.height() - 1) - y, *pix);
        }
        reflected_image
    }

    fn translation(&self, move_x: u32, move_y: u32, background: &str) -> Self {
        let mut translated_image = self.clone();
        translated_image = match background {
            "black" => translated_image.to_black(),
            "white" => translated_image.to_white(),
            "transparent" => translated_image.to_transparent(),
            _ => translated_image.to_black(),
        };
        for (x, y, pix) in self.enumerate_pixels() {
            if x + move_x < self.width() && y + move_y < self.height() {
                translated_image.put_pixel(x + move_x, y + move_y, *pix);
            }
        }
        translated_image
    }
}

impl Morphological for image::GrayImage {
    fn reflection(self) -> Self {
        let mut reflected_image = self.clone();
        for (x, y, pix) in self.enumerate_pixels() {
            reflected_image.put_pixel((self.width() - 1) - x, (self.height() - 1) - y, *pix);
        }
        reflected_image
    }

    fn translation(&self, move_x: u32, move_y: u32, background: &str) -> Self {
        let mut translated_image = self.clone();
        translated_image = match background {
            "black" => translated_image.to_black(),
            "white" => translated_image.to_white(),
            _ => translated_image.to_black(),
        };
        for (x, y, pix) in self.enumerate_pixels() {
            if x + move_x < self.width() && y + move_y < self.height() {
                translated_image.put_pixel(x + move_x, y + move_y, *pix);
            }
        }
        translated_image
    }
}

impl Morphological for image::GrayAlphaImage {
    fn reflection(self) -> Self {
        let mut reflected_image = self.clone();
        for (x, y, pix) in self.enumerate_pixels() {
            reflected_image.put_pixel((self.width() - 1) - x, (self.height() - 1) - y, *pix);
        }
        reflected_image
    }

    fn translation(&self, move_x: u32, move_y: u32, background: &str) -> Self {
        let mut translated_image = self.clone();
        translated_image = match background {
            "black" => translated_image.to_black(),
            "white" => translated_image.to_white(),
            "transparent" => translated_image.to_transparent(),
            _ => translated_image.to_black(),
        };
        for (x, y, pix) in self.enumerate_pixels() {
            if x + move_x < self.width() && y + move_y < self.height() {
                translated_image.put_pixel(x + move_x, y + move_y, *pix);
            }
        }
        translated_image
    }
}

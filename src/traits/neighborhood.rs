use image;

fn not_implemented() {
    panic!("Shit, I'm not implemented yet!");
}

pub trait Neighborhood {
    type ReturnPixel;

    /// Default vertical and horizontal neighborhood of a given pixel
    /// Denoted by N4(p), they are, respectively
    /// (x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1)
    fn four(&self, x: u32, y: u32) -> Vec<Self::ReturnPixel>;

    /// Diagonal neighborhood of a given pixel
    /// Denoted by Nd(p), they are, respectively
    /// (x + 1, y + 1), (x + 1, y - 1), (x - 1, y + 1), (x - 1, y - 1)
    fn four_diagonal(&self, x: u32, y: u32) -> Vec<Self::ReturnPixel>;

    /// A three by three neighborhood of a given pixel
    ///
    /// Given a pixel location (x and y, both u32), it returns a vector
    /// containing all the pixels (in their original container) in the
    /// immediate vicinity, ranging from (x - 1, y - 1) to (x + 1, y + 1),
    /// including the given pixel itself.
    fn three_by_three(&self, x: u32, y: u32) -> Vec<Self::ReturnPixel>;

    /// A five by five neighborhood of a given pixel
    ///
    /// Given a pixel location (x and y, both u32), it returns a vector
    /// containing all the pixels (in their original container) in the
    /// immediate vicinity, ranging from (x - 2, y - 2) to (x + 2, y + 2),
    /// including the given pixel itself.
    fn five_by_five(&self, x: u32, y: u32) -> Vec<Self::ReturnPixel>;

    /// A seven by seven neighborhood of a given pixel
    ///
    /// Given a pixel location (x and y, both u32), it returns a vector
    /// containing all the pixels (in their original container) in the
    /// immediate vicinity, ranging from (x - 3, y - 3) to (x + 3, y + 3),
    /// including the given pixel itself.
    fn seven_by_seven(&self, x: u32, y: u32) -> Vec<Self::ReturnPixel>;
}

impl Neighborhood for image::RgbImage {
    type ReturnPixel = image::Rgb<u8>;

    fn four(&self, x: u32, y: u32) -> Vec<Self::ReturnPixel> {
        let mut four = vec![];
        if x >= 1 && x <= self.width() - 1 {
            four.push(*self.get_pixel(x + 1, y));
            four.push(*self.get_pixel(x - 1, y));
        }
        if y >= 1 && x <= self.height() - 1 {
            four.push(*self.get_pixel(x, y + 1));
            four.push(*self.get_pixel(x, y - 1));
        }
        four
    }

    fn four_diagonal(&self, x: u32, y: u32) -> Vec<Self::ReturnPixel> {
        let mut diagonal_four = vec![];
        if x >= 1 {
            diagonal_four.push(*self.get_pixel(x - 1, y - 1));
        }
        if x <= self.width() - 1 {
            diagonal_four.push(*self.get_pixel(x + 1, y + 1));
        }
        if y >= 1 {
            diagonal_four.push(*self.get_pixel(x + 1, y - 1));
        }
        if y <= self.width() - 1 {
            diagonal_four.push(*self.get_pixel(x - 1, y + 1))
        }
        diagonal_four
    }

    fn three_by_three(&self, x: u32, y: u32) -> Vec<Self::ReturnPixel> {
        let mut result = vec![];
        for i in 0..3 {
            for j in 0..3 {
                result.push(*self.get_pixel((x - 1) + j, (y - 1) + i));
            }
        }
        result
    }

    fn five_by_five(&self, x: u32, y: u32) -> Vec<Self::ReturnPixel> {
        let mut result = vec![];
        for i in 0..5 {
            for j in 0..5 {
                result.push(*self.get_pixel((x - 2) + j, (y - 2) + i));
            }
        }
        result
    }

    fn seven_by_seven(&self, x: u32, y: u32) -> Vec<Self::ReturnPixel> {
        let mut result = vec![];
        for i in 0..7 {
            for j in 0..7 {
                result.push(*self.get_pixel((x - 3) + j, (y - 3) + i));
            }
        }
        result
    }
}

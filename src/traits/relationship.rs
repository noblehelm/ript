fn euclidean(p: (f32, f32), q: (f32, f32)) -> f32 {
    f32::sqrt((p.0 - q.0).powf(2.0) + (p.1 - q.1).powf(2.0))
}
fn cityblock(p: (f32, f32), q: (f32, f32)) -> f32 {
    f32::abs((p.0 - q.0) + (p.1 - q.0))
}
fn chessboard(p: (f32, f32), q: (f32, f32)) -> f32 {
    f32::max(f32::abs(p.0 - q.0), f32::abs(p.1 - q.1))
}

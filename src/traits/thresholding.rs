use image;

pub trait Thresholding {
    fn to_binary(self) -> Self;
}

enum ThresholdingMethod {
    Manual(u32),
    BHT,
    Otsu,
}

impl Thresholding for image::GrayImage {
    fn to_binary(self) -> Self {
        let mut new_image = self.clone();
        for (x, y, pix) in self.enumerate_pixels() {
            let new_pixel = match pix[0] {
                0..=127 => 0,
                128..=255 => 255,
                _ => 0,
            };
            new_image.put_pixel(x, y, image::Luma([new_pixel]))
        }
        new_image
    }
}

impl Thresholding for image::GrayAlphaImage {
    fn to_binary(self) -> Self {
        let mut new_image = self.clone();
        for (x, y, pix) in self.enumerate_pixels() {
            let new_pixel = match pix[0] {
                0..=127 => 0,
                128..=255 => 255,
                _ => 0,
            };
            new_image.put_pixel(x, y, image::LumaA([new_pixel, pix[1]]));
        }
        new_image
    }
}

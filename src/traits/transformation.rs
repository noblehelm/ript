use image;

pub trait Transformation {
    fn negative(&self) -> Self;
    fn log(&self, c: f32) -> Self;
    fn gamma(&self, c: f32, lamb: f32) -> Self;
    fn contrast_stretching(&self, high: u8, low: u8, method: ContrastStretchingMethod) -> Self;
    fn intensity_slicing(&self, range: (u8, u8), method: IntensitySlicingMethod) -> Self;
}

pub enum ContrastStretchingMethod {
    Basic,
    EndInSearch,
}

pub enum IntensitySlicingMethod {
    Binary,
    BinaryWithArgs(u8, u8),
    Unchanged,
    UnchangedWithArgs(u8),
}

fn pixel_log(p: u8, c: f32) -> u8 {
    (c * ((1 + p as u32) as f32).log10()) as u8
}

fn gamma_correction(p: u8, c: f32, y: f32) -> u8 {
    (c * (p as f32).powf(y)) as u8
}

fn basic_contrast_stretching(pix: u8, h: u8, l: u8) -> u8 {
    let mut new_pix = (((pix as f32 - l as f32) / (h as f32 - l as f32)) * 255f32) as u8;
    if new_pix > <u8>::max_value() {
        new_pix = <u8>::max_value();
    }
    new_pix
}

fn end_in_search_contrast_stretching(pix: u8, h: u8, l: u8) -> u8 {
    let mut new_pix = 0;
    if pix <= l {
        new_pix = l;
    } else if pix >= l && pix <= h {
        new_pix = (255f32 * ((pix as f32 - l as f32) / (h as f32 - l as f32))) as u8;
    } else if pix >= h {
        new_pix = h;
    }
    new_pix
}

impl Transformation for image::RgbImage {
    fn negative(&self) -> Self {
        let mut negative_image = self.clone();
        let l = <u8>::max_value();
        for (x, y, pix) in self.enumerate_pixels() {
            negative_image.put_pixel(x, y, image::Rgb([l - pix[0], l - pix[1], l - pix[2]]));
        }
        negative_image
    }

    fn log(&self, c: f32) -> Self {
        let mut log_image = self.clone();
        for (x, y, pix) in self.enumerate_pixels() {
            log_image.put_pixel(
                x,
                y,
                image::Rgb([
                    pixel_log(pix[0], c),
                    pixel_log(pix[1], c),
                    pixel_log(pix[2], c),
                ]),
            );
        }
        log_image
    }

    fn gamma(&self, c: f32, lamb: f32) -> Self {
        let mut gamma_image = self.clone();
        for (x, y, pix) in self.enumerate_pixels() {
            gamma_image.put_pixel(
                x,
                y,
                image::Rgb([
                    gamma_correction(pix[0], c, lamb),
                    gamma_correction(pix[1], c, lamb),
                    gamma_correction(pix[2], c, lamb),
                ]),
            );
        }
        gamma_image
    }

    fn contrast_stretching(&self, _high: u8, _low: u8, _method: ContrastStretchingMethod) -> Self {
        panic!("Shit, I'm not implemented yet..");
    }

    fn intensity_slicing(&self, _range: (u8, u8), _method: IntensitySlicingMethod) -> Self {
        panic!("Shit, I'm not implemented yet..");
    }
}

impl Transformation for image::GrayImage {
    fn negative(&self) -> Self {
        let mut negative_image = self.clone();
        let l = <u8>::max_value();
        for (x, y, pix) in self.enumerate_pixels() {
            negative_image.put_pixel(x, y, image::Luma([l - pix[0]]));
        }
        negative_image
    }

    fn log(&self, c: f32) -> Self {
        let mut log_image = self.clone();
        for (x, y, pix) in self.enumerate_pixels() {
            log_image.put_pixel(x, y, image::Luma([pixel_log(pix[0], c)]));
        }
        log_image
    }

    fn gamma(&self, c: f32, lamb: f32) -> Self {
        let mut new_image = self.clone();
        for (x, y, pix) in self.enumerate_pixels() {
            new_image.put_pixel(x, y, image::Luma([gamma_correction(pix[0], c, lamb)]));
        }
        new_image
    }

    fn contrast_stretching(&self, high: u8, low: u8, method: ContrastStretchingMethod) -> Self {
        let mut new_image = self.clone();
        let _a = self.iter().max();
        let _b = self.iter().min();
        for (x, y, pix) in self.enumerate_pixels() {
            new_image.put_pixel(
                x,
                y,
                image::Luma([match method {
                    ContrastStretchingMethod::Basic => basic_contrast_stretching(pix[0], high, low),
                    ContrastStretchingMethod::EndInSearch => {
                        end_in_search_contrast_stretching(pix[0], high, low)
                    }
                }]),
            );
        }
        new_image
    }

    fn intensity_slicing(&self, range: (u8, u8), method: IntensitySlicingMethod) -> Self {
        let mut new_image = self.clone();
        for (x, y, pix) in self.enumerate_pixels() {
            new_image.put_pixel(
                x,
                y,
                image::Luma([match method {
                    IntensitySlicingMethod::Binary => if range.0 <= pix[0] && pix[0] <= range.1 {
                        <u8>::max_value()
                    } else {
                        <u8>::min_value()
                    },
                    IntensitySlicingMethod::BinaryWithArgs(p, q) => {
                        if range.0 <= pix[0] && pix[0] <= range.1 {
                            p
                        } else {
                            q
                        }
                    }
                    IntensitySlicingMethod::Unchanged => {
                        if range.0 <= pix[0] && pix[0] <= range.1 {
                            <u8>::max_value()
                        } else {
                            pix[0]
                        }
                    }
                    IntensitySlicingMethod::UnchangedWithArgs(p) => {
                        if range.0 <= pix[0] && pix[0] <= range.1 {
                            p
                        } else {
                            pix[0]
                        }
                    }
                }]),
            );
        }
        new_image
    }
}
